#pragma once
using namespace System;
using namespace System::Diagnostics;
using namespace System::Threading;
using namespace System::Text;
using namespace System::Collections::Generic;
/// <summary>
/// Summary for Runexe
/// </summary>

ref class CRunExe
{
public:
	CRunExe();
	Tuple<int, List<String^>^, List<String^>^>^ startRunExe(String^ exepath, String^ sparam, int timeout);
	Tuple<int, List<String^>^, List<String^>^>^ startCmdExe(String^ sparam, int timeout);
	int ProcessRunExe(String^ exepath, String^ sparam, int timeout);

	List<String^>^ getExeStdOut() {
		return stdoutSB;
	}
	List<String^>^ getExeStdErr() {
		return stderrSB;
	}

	Dictionary<String^, String^>^ RunAdbGetProp(String^ adbexe, String^ s, int nTimeout);
private:
	AutoResetEvent^ stdoutEvt;
	AutoResetEvent^ stderrEvt;
	List<String^>^ stdoutSB;
	List<String^>^ stderrSB;
	void StdoutReceived_Handler(Object^ sender, DataReceivedEventArgs^ e);
	void StderrReceived_Handler(Object^ sender, DataReceivedEventArgs^ e);
};
