#pragma once

using namespace System;
using namespace System::IO;
using namespace System::Net;
using namespace System::Threading;

ref class CMuxtexUI
{
public:
	static String^ sMuxName;
	static void EnterMuxUI();
	static void ExitMuxUI();
private:
	static System::Threading::Mutex^ muxUI = nullptr;
};

