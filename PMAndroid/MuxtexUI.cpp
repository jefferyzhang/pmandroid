#include "stdafx.h"
#include "MuxtexUI.h"
#include "clicommon.h"

void CMuxtexUI::EnterMuxUI()
{
	try
	{
		Mutex::TryOpenExisting(sMuxName, muxUI);
	}
	catch (Exception^ e)
	{
		logIt(e->ToString());
	}
	if (muxUI != nullptr)
	{
		logIt(String::Format("Wait Muxtex {0}", sMuxName));
		muxUI->WaitOne(30000);
	}
}
void CMuxtexUI::ExitMuxUI()
{
	if (muxUI != nullptr)
	{
		logIt("Exit Muxtex");
		try
		{
			muxUI->ReleaseMutex();
			//muxUI.Dispose();
			muxUI = nullptr;
		}
		catch (Exception^) {}
	}
}