#pragma once 

using namespace System;
using namespace System::Text::RegularExpressions;
using namespace System::Collections::Generic;

void logIt(String^ s);
void SetExePath(String^ exepath);

Boolean ClearAppData(String^ sId, String^ sBundle);
int ShClearAppData(String^ sSerial, String^ sShFile);
int UnInstallAndroidApp(String^ sId, String^ sBundle);
int ShUnInstallAndroidApp(String^ sId, String^ sFile);
array<String^>^  RunAdbCommand(String^ sId, String^ sCommanline);
String^ CreateTooBox();
int UploadAndroidFile(String^ sId, String^ sPcFileName, String^ sDevFileName);

List<String^>^ GetIgnoreApp(String^ section);
//void ClearAppDataTest(String^ sId);
Boolean ClearAppDataWhite(String^ sId, String^ signore);
int ADBForward(String^ sId, int &a, int b);
int InstallAndroidApp(String^ sId, String^ sPcFileName);
int AndroidTapScreenBy(String^ sId, String^ xmlfilter);

void GetApkFileVersion(Object^ sVersion);