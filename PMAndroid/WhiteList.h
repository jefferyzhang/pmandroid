#pragma once

using namespace System;
using namespace System::Xml;
using namespace System::Collections::Generic;

ref class CConfigData
{
public:
	CConfigData();
	String^ m_sCategory;
	Boolean	m_bReport;
	bool    m_bFind;
	List<String^>^ m_pakList;
};


ref class CWhiteList
{
public:
	CWhiteList(String^ sMaker, String^ sModel, String^ sVer);
	Boolean InitXml();

	void GetXMLList();

	void GetParentData(XmlNode^ node);

	static Dictionary<String^, CConfigData^>^ m_xmlConfig;
	static String^ m_ReadId;

private:
	String^ m_sId;
	String^ m_Maker;
	String^ m_Model;
	String^ m_Version;
	String^ m_sMakerModel;

	XmlDocument^ m_xmlDoc;

};

