// PMAndroid.cpp : main project file.

#include "stdafx.h"
#include <stdio.h>


#include "cheadercommon.h"
#include "MuxtexUI.h"
#include "clicommon.h"
#include "WhiteList.h"


using namespace System;
using namespace System::IO;
using namespace System::Net;
using namespace System::Threading;
using namespace System::Net::Sockets;
using namespace System::Text;


int _label = 0;
int BASE_PORT = 8000;

void logIt(String^ s)
{
	System::Diagnostics::Trace::WriteLine(String::Format("[Label_{0}]: {1}", _label, s));
	Console::WriteLine(s);
}

void downloadFileFromFtp(String^ remoteFilename, String^ localFilename)
{
	System::Threading::Mutex^ m = nullptr;
	bool needReleaseMutex = false;
	try
	{
		m = System::Threading::Mutex::OpenExisting(System::IO::Path::GetFileNameWithoutExtension(localFilename));
	}
	catch (Threading::WaitHandleCannotBeOpenedException^ e)
	{
		m = gcnew Threading::Mutex(false, System::IO::Path::GetFileNameWithoutExtension(localFilename));
	}
	catch (...)
	{
		m = nullptr;
	}
	try
	{
		if (m != nullptr)
			needReleaseMutex = m->WaitOne(10 * 1000);

		System::Boolean needDownload = false;
		if (!System::IO::File::Exists(localFilename))
			needDownload = true;
		else
		{
			System::IO::FileInfo^ info = gcnew System::IO::FileInfo(localFilename);
			System::TimeSpan^ ts = DateTime::Now - info->LastWriteTime;
			if (ts->TotalHours > 2)
			{
				needDownload = true;
			}
		}
		if (needDownload)
		{
			//System::Net::WebClient^ client = gcnew Net::WebClient();
			//client->Credentials = gcnew Net::NetworkCredential(L"fd_clean", L"fd_clean340!");
			//client->DownloadFile(remoteFilename, localFilename);

			// Get the object used to communicate with the server.
			FtpWebRequest^ request = (FtpWebRequest^)WebRequest::Create(remoteFilename);
			request->Method = WebRequestMethods::Ftp::DownloadFile;
			request->UseBinary = true;

			// This example assumes the FTP site uses anonymous logon.
			request->Credentials = gcnew NetworkCredential("fd_clean", "fd_clean340!");

			FtpWebResponse^ response = (FtpWebResponse^)request->GetResponse();
			Stream^ stream = response->GetResponseStream();

			array<byte>^ buffer = gcnew array<byte>(2048);
			FileStream^ fs = gcnew FileStream(localFilename, FileMode::Create);
			int ReadCount = stream->Read(buffer, 0, buffer->Length);
			while (ReadCount > 0)
			{
				fs->Write(buffer, 0, ReadCount);
				ReadCount = stream->Read(buffer, 0, buffer->Length);
			}

			logIt(String::Format("Download Complete, status {0}", response->StatusDescription));

			fs->Close();
			response->Close();
		}
	}
	catch (Exception^ e)
	{
	}
	finally
	{
		if (m != nullptr)
		{
			if (needReleaseMutex)
				m->ReleaseMutex();
			m->Close();
		}
	}
}

int GetAvailablePort(int startingPort)
{
	int nPort = startingPort;

	WSADATA wsa;

	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		printf("Failed. Error Code : %d", WSAGetLastError());
		return nPort;
	}
	SOCKET clientSock;
	clientSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
	struct sockaddr_in sin;
	sin.sin_family = AF_INET; // Internet address family.
	unsigned long ipv4addr = INADDR_NONE;
	InetPton(AF_INET, _T("127.0.0.1"), &ipv4addr);
	sin.sin_addr.s_addr = ipv4addr;
	sin.sin_port = 0; // Port.
	int addrlen = sizeof(sin);
	if (bind(clientSock, (struct sockaddr *) &sin, sizeof(sin)) < 0)
	{
		return nPort;
	}
	if (getsockname(clientSock, (struct sockaddr *)&sin, &addrlen) == 0 &&
		sin.sin_family == AF_INET &&
		addrlen == sizeof(sin))
	{
		nPort = ntohs(sin.sin_port);
	}
	closesocket(clientSock);

	return nPort;
}

int Connect(Int32 port)
{
	int err = ERROR_SUCCESS;
	try
	{
		// Create a TcpClient.
		// Note, for this client to work you need to have a TcpServer 
		// connected to the same address as specified by the server, port
		// combination.
		TcpClient^ client = gcnew TcpClient("127.0.0.1", port);
		// Get a client stream for reading and writing.
		//  Stream stream = client->GetStream();

		NetworkStream^ stream = client->GetStream();
		// Receive the TcpServer::response.
		bool bread = true;
		DateTime dtst = DateTime::Now;
		// String to store the response ASCII representation.
		String^ responseData = String::Empty;
		do{
			// Buffer to store the response bytes.
			array<Byte>^data = gcnew array<Byte>(4096);

			if (stream->DataAvailable) {
				// Read the first batch of the TcpServer response bytes.
				Int32 bytes = stream->Read(data, 0, data->Length);
				
				responseData = Text::Encoding::ASCII->GetString(data, 0, bytes);
				logIt(String::Format("Received: {0}", responseData));
				dtst = DateTime::Now;
			}
			else {
				Sleep(100);
				DateTime now = DateTime::Now;
				if (now.Subtract(dtst).TotalSeconds > 180)
				{
					logIt("recv timeout.");
					err = ERROR_TIMEOUT;
					if (responseData->Contains("progress:")) {
						logIt(responseData + " data handle");
						auto sprogress = responseData->Replace("progress:", "")->Trim();
						if (Convert::ToInt32(sprogress) > 95) {
							bread = false;
							err = ERROR_SUCCESS;
						}
					}
					break;
				}	
			}
			if (responseData->IndexOf("Done:100") != -1){
				bread = false;
			}
		} while (bread);

		if (err == ERROR_TIMEOUT)
		{
			client = gcnew TcpClient("127.0.0.1", port);
			// Get a client stream for reading and writing.
			//  Stream stream = client->GetStream();
			stream = client->GetStream();
		}
		// Translate the passed message into ASCII and store it as a Byte array.
		array<Byte>^data = Text::Encoding::ASCII->GetBytes("finish\n");
		// Send the message to the connected TcpServer. 
		stream->Write(data, 0, data->Length);

		// Close everything.
		client->Close();
	}
	catch (ArgumentNullException^ e)
	{
		logIt(String::Format("ArgumentNullException: {0}", e));
		err = ERROR_EXCEPTION_IN_SERVICE;
	}
	catch (SocketException^ e)
	{
		//[31652] [Label_5]: SocketException: System.Net.Sockets.SocketException (0x80004005): No connection could be made because the target machine actively refused it 127.0.0.1:15310 
		logIt(String::Format("SocketException: {0}", e));
		err = ERROR_NETWORK_NOT_AVAILABLE;
		if (e->ToString()->Contains("the target machine actively refused it")) {
			err = ERROR_RETRY;
		}
	}

	return err;
}
BOOL bDeepClean = false;

int main(array<System::String ^> ^args)
{
	System::Configuration::Install::InstallContext^ _param = gcnew System::Configuration::Install::InstallContext(nullptr, args);

	int nHubport = 0;
	String^ sHubName = "";
	String^ sType = "";
	String^ sSerial = "";
	String^ sShFile = "";
	String^ ignoreData = "";

	GetExePath();


	if (_param->IsParameterTrue("debug"))
	{
		Console::WriteLine("wait for debug. press any key to contiune.");
		Console::ReadKey();
	}
	if (_param->Parameters->ContainsKey("utilpath"))
	{
		SetExePath(_param->Parameters["utilpath"]);
	}
	if (_param->Parameters->ContainsKey("label"))
	{
		_label = Convert::ToInt32(_param->Parameters["label"]);
	}
	if (_param->Parameters->ContainsKey("serail"))
	{
		sSerial = _param->Parameters["serail"];
	}
	if (_param->Parameters->ContainsKey("shfile"))
	{
		sShFile = _param->Parameters["shfile"];
	}
	if (_param->Parameters->ContainsKey("ignore"))
	{
		ignoreData = _param->Parameters["ignore"];
	}

	CMuxtexUI::sMuxName = String::Format("{0}_{1}_UI", _label, sSerial);
	int Ret = 1;
	bDeepClean = _param->IsParameterTrue("ssclean");
	Console::WriteLine(System::DateTime::Now.ToString("HH:mm:ss.fff", System::Globalization::DateTimeFormatInfo::InvariantInfo));
	if (_param->Parameters->ContainsKey("kme")) {
		BOOL bCmdRun = false;
		BOOL bFind = false;
		Regex^ rx = gcnew Regex("com\\..*?\\.knox\\.kccagent$",
			RegexOptions::Compiled | RegexOptions::IgnoreCase);
		array<String^>^ retdata = RunAdbCommand(sSerial, "pm list packages");
		if (retdata->Length > 0) {
			if (retdata[0]->IndexOf("package:") >= 0) {
				bCmdRun = true; //com.sec.knox.kccagent
				for each(auto s in retdata) {
					if (rx->IsMatch(s))
					{
						bFind = true;
						break;
					}
				}
			}
		}
		if (!bCmdRun) {
			retdata = RunAdbCommand(sSerial, "sh -c 'cmd package list packages'");
			if (retdata->Length > 0) {
				if (retdata[0]->IndexOf("package:") >= 0) {
					bCmdRun = true; //com.sec.knox.kccagent
					for each(auto s in retdata) {
						if (rx->IsMatch(s))
						{
							bFind = true;
							break;
						}
					}
				}
			}
		}
		if (bCmdRun) {
			Console::WriteLine("kme="+bFind);
		}
		else {
			Console::WriteLine("kme=error");
		}
		return 0;
	}
	else {
		logIt("other tools download pmandroidwhitelistV1.xml");
		//downloadFileFromFtp(L"ftp://ftp2.futuredial.com/AndroidConfig/pmandroidwhitelistV1.xml",
		//	System::IO::Path::Combine(System::IO::Path::GetDirectoryName(System::Diagnostics::Process::GetCurrentProcess()->MainModule->FileName), L"pmandroidwhitelistV1.xml"));
	}

	if (_param->Parameters->ContainsKey("uninstallapp"))
	{
		if (String::IsNullOrEmpty(sShFile))
		{
			//sShFile = System::IO::Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "apkuninstall.sh");
		}
		if (System::IO::File::Exists(sShFile))
		{
			Ret = ShUnInstallAndroidApp(sSerial, sShFile);
		}
		else
		{
			if (bDeepClean) {
				ignoreData += ";com.futuredial.androidclean";
				ParameterizedThreadStart ^ threadStart = gcnew ParameterizedThreadStart(GetApkFileVersion);
				Thread ^ versionThread = gcnew Thread(threadStart);	
				StringBuilder^  sb = gcnew StringBuilder();
				versionThread->Start(sb);
			}
			Ret = UnInstallAndroidApp(sSerial, ignoreData);
		}
		sShFile = "";
	}
	if (_param->Parameters->ContainsKey("clearappdata"))
	{
		if (String::IsNullOrEmpty(sShFile))
		{
			//sShFile = System::IO::Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "apklist.sh");
		}
		if (System::IO::File::Exists(sShFile))
		{
			Ret = ShClearAppData(sSerial, sShFile);
		}
		else
		{
			//Ret = ClearAppData(sSerial, ignoreData) ? 0 : 2;
			//ClearAppDataTest(sSerial);
			Ret = ClearAppDataWhite(sSerial, ignoreData) ? 0 : 2;
		}
		sShFile = "";
	}
	if (_param->Parameters->ContainsKey("killsdcard"))
	{
		array<String^>^ retdata= RunAdbCommand(sSerial, "rm -fr $EXTERNAL_STORAGE/*");
		if (retdata->Length != 0)
		{
			String^ stemp = CreateTooBox();
			if (System::IO::File::Exists(stemp))
			{
				UploadAndroidFile(sSerial, stemp, "/data/local/tmp/toolbox");
				RunAdbCommand(sSerial, "chmod 777 /data/local/tmp/toolbox");
				RunAdbCommand(sSerial, "/data/local/tmp/toolbox rm -fr $EXTERNAL_STORAGE/*");
			}
		}
	}
	if (_param->IsParameterTrue("ssclean"))
	{
		//install app
		String^ sapk = System::IO::Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "androidclean.apk");
		if (!System::IO::File::Exists(sapk))
		{
			logIt("androidclean.apk not found.");
			return ERROR_FILE_NOT_FOUND;
		}
		Ret = InstallAndroidApp(sSerial, sapk);
		//adb shell appops set --uid com.futuredial.androidclean MANAGE_EXTERNAL_STORAGE allow
		auto listreturn = RunAdbCommand(sSerial, "appops set --uid com.futuredial.androidclean MANAGE_EXTERNAL_STORAGE allow");
		

		//adb forward tcp:8000 tcp:9000
		int nPort = GetAvailablePort(BASE_PORT + _label);
		ADBForward(sSerial, nPort, 9000);
		String^ sName = "NewZealand";
		if (_param->Parameters->ContainsKey("name")) sName = _param->Parameters["name"];
		//adb shell am start -n com.futuredial.androidclean/.MainActivity --es "runclear" NewZealand
		CMuxtexUI::EnterMuxUI();
		array<String^>^ sss = RunAdbCommand(sSerial, String::Format("am start -n com.futuredial.androidclean/.MainActivity --es runclear {0}", sName));
		for each(String^ s in sss)
		{
			if (s->IndexOf("failed", StringComparison::CurrentCultureIgnoreCase) > 0 ||
				s->IndexOf("error", StringComparison::CurrentCultureIgnoreCase) > 0 || 
				s->IndexOf("runexe return")>0)
			{
				Ret = ERROR_PORT_NOT_SET;
				logIt("start activity failed: "+ String::Join("*#",sss));
				break;
			}
		}

		/*if (listreturn->Length == 0) {
			RunAdbCommand(sSerial, "input keyevent 4");
		}*/

		//may be need trust
		AndroidTapScreenBy(sSerial, "//node[@resource-id='com.android.permissioncontroller:id/continue_button'];//node[@resource-id='com.android.permissioncontroller:id/permission_allow_button']");//samsung allow

		//tcpclient read info
		if (Ret == ERROR_SUCCESS) {
			Ret = Connect(nPort);
			if (Ret == ERROR_RETRY) {
				ADBForward(sSerial, nPort, 9000);
				Ret = Connect(nPort);
			}
		}

		CMuxtexUI::ExitMuxUI();
		RunAdbCommand(sSerial, "rm /sdcard/clean.dat");
	}
	
	Console::WriteLine(System::DateTime::Now.ToString("HH:mm:ss.fff", System::Globalization::DateTimeFormatInfo::InvariantInfo));
    return Ret;
}
