#include "stdafx.h"
#include "RunExe.h"
#include "clicommon.h"

using namespace System::IO;


void logit(String^ s) {
	Trace::WriteLine("[PMAndroid]: "+s);
}

CRunExe::CRunExe()
{
	stdoutSB = gcnew List<String^>();
	stderrSB = gcnew List<String^>();
	stdoutEvt = gcnew AutoResetEvent(false);
	stderrEvt = gcnew AutoResetEvent(false);
}
void CRunExe::StdoutReceived_Handler(Object^ sender, DataReceivedEventArgs^ e)
{
	if (e->Data == nullptr)
	{
		stdoutEvt->Set();
	}
	else
	{
		logit(String::Format("[stdout]: {0}", e->Data));
		stdoutSB->Add(e->Data);
	}
}
void CRunExe::StderrReceived_Handler(Object^ sender, DataReceivedEventArgs^ e)
{
	if (e->Data == nullptr)
	{
		stderrEvt->Set();
	}
	else
	{
		logit(String::Format("[stderr]: {0}", e->Data));
		stderrSB->Add(e->Data);
	}
}

Tuple<int, List<String^>^, List<String^>^>^ CRunExe::startCmdExe(String^ sparam, int timeout)
{
	int retI = ERROR_TIMEOUT;
	logit(String::Format("startCmdExe: ++ param: {0} timeout: {1}", sparam, timeout));
	Process^ p = gcnew Process();
	p->StartInfo->FileName = "cmd.exe";
	p->StartInfo->Arguments = String::Format("/C \"{0}\"", sparam);
	//p->StartInfo->WorkingDirectory = System::IO::Path::GetDirectoryName(exepath);
	p->StartInfo->UseShellExecute = false;
	p->StartInfo->CreateNoWindow = true;
	p->StartInfo->RedirectStandardError = true;
	p->StartInfo->RedirectStandardOutput = true;
	p->OutputDataReceived += gcnew DataReceivedEventHandler(this, &CRunExe::StdoutReceived_Handler);
	p->ErrorDataReceived += gcnew DataReceivedEventHandler(this, &CRunExe::StderrReceived_Handler);
	p->Start();
	p->BeginErrorReadLine();
	p->BeginOutputReadLine();
	if (p->WaitForExit(timeout * 2))
	{
		// process exit.
		retI = p->ExitCode;
		// wait for pipe complete.
		EventWaitHandle::WaitAll(gcnew array<AutoResetEvent^>{stdoutEvt, stderrEvt}, timeout);
	}
	else
	{
		// timeout
		p->Kill();
		retI = ERROR_TIMEOUT;
	}
	logit(String::Format("startCmdExe: ++ ret: {0} ", retI));
	return gcnew Tuple<int, List<String^>^, List<String^>^>(retI, stdoutSB, stderrSB);
}

Tuple<int, List<String^>^, List<String^>^>^ CRunExe::startRunExe(String^ exepath, String^ sparam, int timeout)
{
	int retI = ERROR_TIMEOUT;
	logit(String::Format("startRunExe: ++ exe: {0} param: {1} timeout: {2}", exepath, sparam, timeout));
	Process^ p = gcnew Process();
	p->StartInfo->FileName = exepath;
	p->StartInfo->Arguments = sparam;
	p->StartInfo->WorkingDirectory = System::IO::Path::GetDirectoryName(exepath);
	p->StartInfo->UseShellExecute = false;
	p->StartInfo->CreateNoWindow = true;
	p->StartInfo->RedirectStandardError = true;
	p->StartInfo->RedirectStandardOutput = true;
	p->OutputDataReceived += gcnew DataReceivedEventHandler(this, &CRunExe::StdoutReceived_Handler);
	p->ErrorDataReceived += gcnew DataReceivedEventHandler(this, &CRunExe::StderrReceived_Handler);
	p->Start();
	p->BeginErrorReadLine();
	p->BeginOutputReadLine();
	if (p->WaitForExit(timeout * 2))
	{
		// process exit.
		retI = p->ExitCode;
		// wait for pipe complete.
		EventWaitHandle::WaitAll(gcnew array<AutoResetEvent^>{stdoutEvt, stderrEvt}, timeout);
	}
	else
	{
		// timeout
		p->Kill();
		retI = ERROR_TIMEOUT;
	}
	logit(String::Format("startRunExe: ++ ret: {0} ", retI));
	return gcnew Tuple<int, List<String^>^, List<String^>^>(retI, stdoutSB, stderrSB);
}

int CRunExe::ProcessRunExe(String^ exepath, String^ sparam, int timeout) {
	int retI = ERROR_TIMEOUT;
	logit(String::Format("ProcessRunExe: ++ exe: {0} param: {1} timeout: {2}", exepath, sparam, timeout));
	Process^ p = gcnew Process();
	p->StartInfo->FileName = exepath;
	p->StartInfo->Arguments = sparam;
	p->StartInfo->WorkingDirectory = System::IO::Path::GetDirectoryName(exepath);
	p->StartInfo->UseShellExecute = false;
	p->StartInfo->CreateNoWindow = true;
	p->StartInfo->RedirectStandardError = true;
	p->StartInfo->RedirectStandardOutput = true;
	p->OutputDataReceived += gcnew DataReceivedEventHandler(this, &CRunExe::StdoutReceived_Handler);
	p->ErrorDataReceived += gcnew DataReceivedEventHandler(this, &CRunExe::StderrReceived_Handler);
	p->Start();
	p->BeginErrorReadLine();
	p->BeginOutputReadLine();
	if (p->WaitForExit(timeout * 2))
	{
		// process exit.
		retI = p->ExitCode;
		// wait for pipe complete.
		EventWaitHandle::WaitAll(gcnew array<AutoResetEvent^>{stdoutEvt, stderrEvt}, timeout);
	}
	else
	{
		// timeout
		p->Kill();
		retI = ERROR_TIMEOUT;
	}
	logit(String::Format("startRunExe: ++ ret: {0} ", retI));
	return retI;
}

Dictionary<String^, String^>^ CRunExe::RunAdbGetProp(String^ adbexe, String^ s, int nTimeout) {
	Dictionary<String^, String^>^ ret = gcnew Dictionary<String^, String^>();
	String^ sParam = " shell getprop";
	if (!String::IsNullOrEmpty(s))
	{
		sParam = " -s " + s + sParam;
	}
	int nret = ProcessRunExe(adbexe, sParam, nTimeout);
	if (nret == ERROR_SUCCESS) {
		for each(String^ ss in stdoutSB->ToArray())
		{
			String^ sPatten = "\\[(.*)]:\\s*\\[(.*)\\]";
			Regex^ r1 = gcnew Regex(sPatten, RegexOptions::IgnoreCase);
			Match^ match = r1->Match(ss);
			if (match->Success)
			{
				String^ k = match->Groups[1]->Value;
				String^ v = match->Groups[2]->Value;
				try
				{
					ret->Add(k, v);
				}
				catch (Exception^)
				{

				}
			}
		}
	}
	return ret;
}