#include "stdafx.h"
#include "WhiteList.h"

CConfigData::CConfigData()
{
	m_sCategory = "";
	m_bReport = true;
	m_bFind = false;
	m_pakList = gcnew List<String^>();
}

//////////////////////////////////////////////////////////////////////////////////////////
CWhiteList::CWhiteList(String^ sMaker, String^ sModel, String^ sVer)
{
	m_sId = String::Format("{0}_{1}_{2}", sMaker,
		sModel,
		sVer);
	m_ReadId = m_sId;
	m_Maker = sMaker;
	m_Model = sModel;
	m_Version = sVer;
	m_sMakerModel = String::Format("{0}_{1}", sMaker,
		sModel);
	m_xmlConfig = gcnew Dictionary<String^, CConfigData^>(StringComparer::OrdinalIgnoreCase);
}

Boolean CWhiteList::InitXml()
{
	Boolean bRet = false;
	String^ sConfigxml = System::IO::Path::Combine(AppDomain::CurrentDomain->BaseDirectory,"pmandroidwhitelistV1.xml");
	if (System::IO::File::Exists(sConfigxml))
	{
		m_xmlDoc = gcnew XmlDocument();
		try{
			m_xmlDoc->Load(sConfigxml);
			bRet = true;
		}
		catch (Exception^)
		{
			bRet = false;
		}
	}

	return bRet;
}

void CWhiteList::GetParentData(XmlNode^ node)
{
	if (node != nullptr)
	{
		if (node->Attributes["parentid"] != nullptr)
		{
			String^ sAttrib = node->Attributes["parentid"]->Value;
			if (!String::IsNullOrEmpty(sAttrib))
			{
				XmlNode^ idNode = m_xmlDoc->SelectSingleNode(String::Format("/pmandroid/device[@id='{0}']", sAttrib));
				if (idNode != nullptr)
				{
					GetParentData(idNode);
				}
			}
			for each(XmlNode^ childNode in node->ChildNodes)
			{
				if (childNode->NodeType == XmlNodeType::Element)
				{
					String^ sCateGory = "";
					if (String::Compare(childNode->Name, "apps", true) == 0 && childNode->Attributes["category"] != nullptr)
					{
						sCateGory = childNode->Attributes["category"]->Value;
					}
					else
						continue;

					CConfigData^ data;
					if (m_xmlConfig->ContainsKey(sCateGory))
					{
						data = m_xmlConfig[sCateGory];
					}
					else
					{
						data = gcnew CConfigData();
						data->m_sCategory = sCateGory;
						m_xmlConfig->Add(sCateGory, data);
					}

					if (childNode->Attributes["report"] != nullptr)
					{
						data->m_bReport = Convert::ToBoolean(childNode->Attributes["report"]->Value);
					}

					for each(XmlNode^ ccNode in childNode->ChildNodes)
					{
						if (ccNode->NodeType == XmlNodeType::Element)
						{
							if (ccNode->Attributes["op"] != nullptr && String::Compare("remove", ccNode->Attributes["op"]->Value) == 0)
							{
								//ret->Remove(ccNode->InnerText);
								data->m_pakList->Remove(ccNode->InnerText);
							}
							else
							{
								data->m_pakList->Add(ccNode->InnerText);
							}
						}
					}
				}
			}
		}
	}
}

void CWhiteList::GetXMLList()
{
	String^ sPath;
	XmlNode^ idNode;
	if (m_xmlDoc != nullptr)
	{
		if (!String::IsNullOrWhiteSpace(m_sId)) {
			sPath = String::Format("/pmandroid/device[@id='{0}']", m_sId);
			idNode = m_xmlDoc->SelectSingleNode(sPath);
		}
		if (idNode == nullptr && !String::IsNullOrWhiteSpace(m_sMakerModel)) {
			sPath = String::Format("/pmandroid/device[@id='{0}']", m_sMakerModel);
			idNode = m_xmlDoc->SelectSingleNode(sPath);
		}
		if (idNode == nullptr && !String::IsNullOrWhiteSpace(m_Maker)) {
			sPath = String::Format("/pmandroid/device[@id='{0}']", m_Maker);
			idNode = m_xmlDoc->SelectSingleNode(sPath);
		}
		if (idNode == nullptr)
		{
			idNode = m_xmlDoc->SelectSingleNode("/pmandroid/device[@id='common']");
			if (idNode != nullptr)
			{
				//for each(XmlNode^ childNode in idNode->ChildNodes)
				//{
				//	if (childNode->NodeType == XmlNodeType::Element)
				//	{
				//		if (childNode->Attributes["op"] != nullptr && String::Compare("remove", childNode->Attributes["op"]->Value) == 0)
				//		{
				//			ret->Remove(childNode->InnerText);
				//		}
				//		else
				//		{
				//			ret->Add(childNode->InnerText);
				//		}
				//	}
				//}
				for each(XmlNode^ childNode in idNode->ChildNodes)
				{
					if (childNode->NodeType == XmlNodeType::Element)
					{
						String^ sCateGory = "";
						if (String::Compare(childNode->Name, "apps", true) == 0 && childNode->Attributes["category"] != nullptr)
						{
							sCateGory = childNode->Attributes["category"]->Value;
						}
						else
							continue;

						CConfigData^ data;
						if (m_xmlConfig->ContainsKey(sCateGory))
						{
							data = m_xmlConfig[sCateGory];
						}
						else
						{
							data = gcnew CConfigData();
							data->m_sCategory = sCateGory;
							m_xmlConfig->Add(sCateGory, data);
						}

						if (childNode->Attributes["report"] != nullptr)
						{
							data->m_bReport = Convert::ToBoolean(childNode->Attributes["report"]->Value);
						}

						for each(XmlNode^ ccNode in childNode->ChildNodes)
						{
							if (ccNode->NodeType == XmlNodeType::Element)
							{
								if (ccNode->Attributes["op"] != nullptr && String::Compare("remove", ccNode->Attributes["op"]->Value) == 0)
								{
									//ret->Remove(ccNode->InnerText);
									data->m_pakList->Remove(ccNode->InnerText);
								}
								else
								{
									data->m_pakList->Add(ccNode->InnerText);
								}
							}
						}
					}
				}

			}
		}
		else
		{
			GetParentData(idNode);
		}
	}

}