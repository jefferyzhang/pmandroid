#include "stdafx.h"
#include "resource.h"
#include "cheadercommon.h"
#include "clicommon.h"
#include "RunExe.h"
#include <msclr\marshal.h>
#include <msclr\marshal_cppstd.h>
#include <vcclr.h>
#include "WhiteList.h"


#define ADBEXE				L"adb.exe"

TCHAR adbpath[MAX_PATH] = { 0 };

int nEchoType = 0;


List<String^>^ GetIgnoreApp(String^ section)
{
	List<String^>^ ret = gcnew List<String^>();
	TCHAR keyname[10] = { 0 };
	int idex = 1;
	String^ sApsthome = Environment::GetEnvironmentVariable("APSTHOME");
	String^ inifile = System::IO::Path::Combine(sApsthome, "androidfile.ini");
	if (System::IO::File::Exists(inifile))
	{
		pin_ptr<const WCHAR> lpFileName = PtrToStringChars(inifile);
		pin_ptr<const WCHAR> lpSection = PtrToStringChars(section);
		do
		{
			TCHAR ReturnedString[1024] = { 0 };
			DWORD nSize = 1024;
			_snwprintf_s(keyname, 10, _T("%d"), idex++);
			if (GetPrivateProfileString(
				(LPCTSTR)lpSection,
				keyname,
				_T(""),
				ReturnedString,
				nSize,
				(LPCTSTR)lpFileName
				) > 0)
			{
				ret->Add(gcnew String(ReturnedString));
			}
			else
				break;

		} while (true);

	}
	return ret;
}


void GetExePath()
{
	String^ sApsthome = Environment::GetEnvironmentVariable("APSTHOME");
	if (_tcslen(adbpath) == 0 || !PathFileExists(adbpath))
	{
		//pin_ptr<WCHAR> content = PtrToStringChars(curItem);
		//std::wstring result(content, curItem->Length);
		if (System::IO::File::Exists(System::IO::Path::Combine(sApsthome, ADBEXE)))
		{
			pin_ptr<const TCHAR> content = PtrToStringChars(System::IO::Path::Combine(sApsthome, ADBEXE));
			_snwprintf_s(adbpath, MAX_PATH, _T("%ws"), content);
		}
		else if (System::IO::File::Exists(System::IO::Path::Combine(System::Reflection::Assembly::GetEntryAssembly()->Location, ADBEXE)))
		{
			//System.Reflection.Assembly.GetEntryAssembly().Location;
			pin_ptr<const TCHAR> content = PtrToStringChars(System::IO::Path::Combine(System::Reflection::Assembly::GetEntryAssembly()->Location, ADBEXE));
			_snwprintf_s(adbpath, MAX_PATH, _T("%ws"), content);
		}
		else
		{
			//logIt("Can not find adb.exe! please check");
		}
	}

}

String^ CreateTooBox()
{
	String^ sRet;

	BYTE *pBuf = NULL;
	int nLen = 0;
	if (Utiltools::LoadResourceToBuf(pBuf, nLen, MAKEINTRESOURCE(IDR_TOOLBOX), L"TOOL") == ERROR_SUCCESS)
	{
		try{
			String^ stempfile = System::IO::Path::GetTempFileName();
			System::IO::FileStream^ fs = gcnew System::IO::FileStream(stempfile, System::IO::FileMode::OpenOrCreate, System::IO::FileAccess::ReadWrite);
			array<byte>^ _Data = gcnew array<byte>(nLen);
			System::Runtime::InteropServices::Marshal::Copy(IntPtr((void *)pBuf), _Data, 0, nLen);
			fs->Write(_Data, 0, nLen);
			fs->Close();
			delete pBuf;
			sRet = stempfile;
		}
		catch (...)
		{

		}

	}

	return sRet;
}

void SetExePath(String^ exepath)
{
	if (System::IO::File::Exists(exepath))
	{
		if (String::Compare(System::IO::Path::GetFileName(exepath),ADBEXE, TRUE) == 0)		
		{
			pin_ptr<const TCHAR> content = PtrToStringChars(exepath);
			_snwprintf_s(adbpath, exepath->Length, _T("%ws"), content);
		}
	}
}

int ShClearAppData(String^ sId, String^ sShFile)
{
	int ret = ERROR_PACKAGES_IN_USE;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sPhoneFile = "/data/local/tmp/fdclappdata.sh";
	String^ sparm = String::Format(" push \"{0}\" {1}", sShFile, sPhoneFile);
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	if ((ret = runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000)) == ERROR_SUCCESS)
	{
		sparm = String::Format(" shell chmod 777 {0};{0}", sPhoneFile);
		if (!String::IsNullOrEmpty(sId))
		{
			sparm = String::Format("-s {0} ", sId) + sparm;
		}
		if ((ret = runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000)) == ERROR_SUCCESS)
		{
			for each (String^ s in runExe->getExeStdOut()->ToArray())
			{
				logIt("clear data logs:==>" + s);
			}
		}
	}
	return ret;
}


List<String^>^ GetWhiteListFromXML(String^ sId)
{
	List<String^>^ ret = gcnew List<String^>();
	Dictionary<String^, String^>^ dict = (gcnew CRunExe())->RunAdbGetProp(gcnew String(adbpath), sId, 130000);
	/*String^ sfull = String::Format("{0}_{1}_{2}", dict->ContainsKey("ro.product.manufacturer")?dict["ro.product.manufacturer"]:"_",
		dict->ContainsKey("ro.product.model") ? dict["ro.product.model"] : "_", 
		dict->ContainsKey("ro.build.version.release") ? dict["ro.build.version.release"] : "_");
	String^ sMakerModel = String::Format("{0}_{1}_{2}", dict->ContainsKey("ro.product.manufacturer") ? dict["ro.product.manufacturer"] : "_",
		dict->ContainsKey("ro.product.model") ? dict["ro.product.model"] : "_");
	String^ sMaker = String::Format("{0}_{1}_{2}", dict->ContainsKey("ro.product.manufacturer") ? dict["ro.product.manufacturer"] : "_",
		dict->ContainsKey("ro.product.model") ? dict["ro.product.model"] : "_");*/

	CWhiteList^ wlist = gcnew CWhiteList((dict->ContainsKey("ro.product.manufacturer") ? dict["ro.product.manufacturer"] : "_")->ToLower(),
		dict->ContainsKey("ro.product.model") ? dict["ro.product.model"] : "_", 
		dict->ContainsKey("ro.build.version.release") ? dict["ro.build.version.release"] : "_");
	if (wlist->InitXml())
	{
		wlist->GetXMLList();
	}

	return ret;
}

Boolean CheckTheWhileList(String^ spackageName)
{
	Boolean bRet = false;
	if (CWhiteList::m_xmlConfig == nullptr) return bRet;
	if (CWhiteList::m_xmlConfig->Count == 0)
	{
		bRet = true;
	}
	else
	{
		for each(String^ key in CWhiteList::m_xmlConfig->Keys)
		{
			CConfigData^ data = CWhiteList::m_xmlConfig[key];
			if (data->m_pakList->Contains(spackageName))
			{
				bRet = true;
				data->m_bFind = true;
				break;
			}
		}
	}


	return bRet;
}

void OutPutConsole()
{
	if (CWhiteList::m_xmlConfig == nullptr || CWhiteList::m_xmlConfig->Count == 0)
	{
		Console::WriteLine("whitelistignore=not config xml.");
	}
	else
	{
		String^ sResult = "";
		for each(String^ key in CWhiteList::m_xmlConfig->Keys)
		{
			CConfigData^ data = CWhiteList::m_xmlConfig[key];
			if (data->m_bReport && !data->m_bFind)
			{
				sResult += data->m_sCategory + ";";
			}
		}
		Console::WriteLine("whitelistignore=" + sResult);
	}
}

Boolean ClearAppDataWhite(String^ sId, String^ ignoredata)
{
	Boolean bRet = FALSE;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" shell pm list packages");
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}

	List<String^>^ whlist = GetWhiteListFromXML(sId);

	String^ delimStr = ";:";
	array<Char>^ delimiter = delimStr->ToCharArray();
	array<String^>^ ignoresps = ignoredata->Split(delimiter, StringSplitOptions::RemoveEmptyEntries);

	List<String^>^ configignore = GetIgnoreApp("pmandroid_data");
	configignore->AddRange(ignoresps);

	if (runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000) == ERROR_SUCCESS)
	{
		//		Sleep(2000);
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt("find data:" + s);
			array<Char>^ chars = { ':' };
			array<String^>^ sps = s->Split(chars, StringSplitOptions::RemoveEmptyEntries);
			if (sps->Length == 2)
			{
				//if (Array::IndexOf(ignoresps, sps[1]) > -1)
				if (configignore->Contains(sps[1]))
				{
					logIt("Clear ignore app, It is include Black list. " + sps[1]);
					continue;
				}


				//if (whlist->Count > 0 && !whlist->Contains(sps[1]))
				if (!CheckTheWhileList(sps[1]))
				{
					logIt("Clear ignore app, Not include white List. " + sps[1]);
					continue;
				}

				sparm = String::Format(" shell pm clear {0}", sps[1]);
				if (!String::IsNullOrEmpty(sId))
				{
					sparm = String::Format("-s {0} ", sId) + sparm;
				}
				Boolean bRetry = false;
				DWORD dtstart = GetTickCount();
				do
				{
					bRetry = false;
					if (runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000) == ERROR_SUCCESS)
					{
						for each (String^ s in runExe->getExeStdOut()->ToArray())
						{
							//error: device 'TA9921CUVC' not found
							//error: device not found
							String^ sPatten = "error: device.*?not found";
							Regex^ reg = gcnew Regex(sPatten, RegexOptions::IgnoreCase);
							Match^ match = reg->Match(s);
							if (match->Success)
							{
								logIt(s + " device remove. retry...");
								bRetry = true;
								Sleep(5000);
								break;
							}
							Console::WriteLine(sps[1] + "=" + s);
							logIt("clear data status:" + sps[1] + "==>" + s);
						}
					}
					else
					{
						for each (String^ s in runExe->getExeStdErr()->ToArray())
						{
							//error: device 'TA9921CUVC' not found
							//error: device not found
							String^ sPatten = "error: device.*?not found";
							Regex^ reg = gcnew Regex(sPatten, RegexOptions::IgnoreCase);
							Match^ match = reg->Match(s);
							if (match->Success)
							{
								logIt(s + " device remove. retry...");
								bRetry = true;
								Sleep(5000);
								break;
							}
							logIt("clear data status:" + sps[1] + "==>" + s);
						}
					}
				} while (bRetry && (GetTickCount() - dtstart < 120 * 1000));

				if (bRetry)
				{
					logIt("Device not connected. wait..., but not found.");
					bRet = false;
					break;
				}
			}
		}
	}


	OutPutConsole();

	return bRet;

}

Boolean ClearAppData(String^ sId, String^ ignoredata)
{
	//adb -s shell pm list packages | findstr ""

	Boolean bRet = FALSE;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" shell pm list packages");
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId)+sparm;
	}
	
	String^ delimStr = ";:";
	array<Char>^ delimiter = delimStr->ToCharArray();
	array<String^>^ ignoresps = ignoredata->Split(delimiter, StringSplitOptions::RemoveEmptyEntries);

	List<String^>^ configignore = GetIgnoreApp("pmandroid_data");
	configignore->AddRange(ignoresps);

	if (runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000) == ERROR_SUCCESS)
	{
//		Sleep(2000);
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt("find data:"+s);
			array<Char>^ chars = { ':' };
			array<String^>^ sps = s->Split(chars, StringSplitOptions::RemoveEmptyEntries);
			if (sps->Length == 2)
			{
				//if (Array::IndexOf(ignoresps, sps[1]) > -1)
				if (configignore->Contains(sps[1]))
				{
					logIt("Clear ignore app " + sps[1]);
					continue;
				}

				sparm = String::Format(" shell pm clear {0}", sps[1]);
				if (!String::IsNullOrEmpty(sId))
				{
					sparm = String::Format("-s {0} ", sId) + sparm;
				}
				Boolean bRetry = false;
				DWORD dtstart = GetTickCount();
				do
				{
					bRetry = false;
					if (runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000) == ERROR_SUCCESS)
					{
						for each (String^ s in runExe->getExeStdOut()->ToArray())
						{
							//error: device 'TA9921CUVC' not found
							//error: device not found
							String^ sPatten = "error: device.*?not found";
							Regex^ reg = gcnew Regex(sPatten, RegexOptions::IgnoreCase);
							Match^ match = reg->Match(s);
							if (match->Success)
							{
								logIt(s + " device remove. retry...");
								bRetry = true;
								Sleep(5000);
								break;
							}
							Console::WriteLine(sps[1] + "=" + s);
							logIt("clear data status:" + sps[1] + "==>" + s);
						}
					}
					else
					{
						for each (String^ s in runExe->getExeStdErr()->ToArray())
						{
							//error: device 'TA9921CUVC' not found
							//error: device not found
							String^ sPatten = "error: device.*?not found";
							Regex^ reg = gcnew Regex(sPatten, RegexOptions::IgnoreCase);
							Match^ match = reg->Match(s);
							if (match->Success)
							{
								logIt(s + " device remove. retry...");
								bRetry = true;
								Sleep(5000);
								break;
							}
							logIt("clear data status:" + sps[1] + "==>" + s);
						}
					}
				} while (bRetry && (GetTickCount() - dtstart < 120*1000));

				if (bRetry)
				{
					logIt("Device not connected. wait..., but not found.");
					bRet = false;
					break;
				}
			}
		}
	}
	
	return bRet;
}

int ADBForwardList(String^ sId, int &a) {
	int nRet = ERROR_INVALID_PARAMETER;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" forward --remove-all");
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	auto retdata = runExe->startRunExe(gcnew String(adbpath), sparm, 30000);
	nRet = retdata->Item1;
	if (nRet == ERROR_SUCCESS)
	{//1250eec5 tcp:15310 tcp:9000
		Regex^ reg = gcnew Regex(".*?tcp:(\\d+)\\s+tcp:\\d+");
		for each (auto item in retdata->Item2)
		{
			if (!String::IsNullOrWhiteSpace(item)) {
				if (item->Contains(sId)) {
					auto m = reg->Match(item);
					if (m->Success) {
						a = Convert::ToInt32(m->Groups[1]->Value);
						nRet = 10;
						break;
					}
				}
			}
		}
	}
	
	return nRet;
}

int ADBForward(String^ sId, int &a, int b)
{
	int nRet = ERROR_INVALID_PARAMETER;

	if (ADBForwardList(sId, a) == 10) {
		return ERROR_SUCCESS;
	}

	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" forward tcp:{0} tcp:{1}", a, b);
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	auto retdata = runExe->startRunExe(gcnew String(adbpath), sparm, 30000);
	nRet = retdata->Item1;
	if (nRet == ERROR_SUCCESS)
	{
		
	}
	else
	{
		for each (String^ s in retdata->Item3->ToArray())
		{
			logIt(s);
		}
	}
	return nRet;
}

void GetApkFileVersion(Object^ outsb) {
	int nRet = ERROR_INVALID_PARAMETER;
	StringBuilder^ sbret = (StringBuilder^)outsb;
	String^ sapk = System::IO::Path::Combine(System::AppDomain::CurrentDomain->BaseDirectory, "androidclean.apk");
	CRunExe^ runExe = gcnew CRunExe();//command = "aapt.exe dump badging fdbox722.apk | findstr version";
	String^ sparm = String::Format("{0}\\aapt.exe dump badging \"{1}\" | findstr version", Environment::ExpandEnvironmentVariables("%apsthome%androidTools"), sapk);
	String^ sVersion = "";
	auto retdata = runExe->startCmdExe(sparm, 30000);
	nRet = retdata->Item1;
	//package: name='com.futuredial.fdbox722' versionCode='1' versionName='1.0' compileSdkVersion='28' compileSdkVersionCodename='9'
	if (nRet == ERROR_SUCCESS)
	{
		for each(auto line in retdata->Item2) {
			logIt(line);
			int a = line->IndexOf("versionName='");
			if (a > 0) {
				String^ sb = line->Substring(a + (gcnew String("versionName='"))->Length);
				a = sb->IndexOf("'");
				if (a > 0) {
					sVersion = sb->Substring(0, a);
					logIt("androidclean version="+sVersion);
					sbret->Append(sVersion);
				}
			}
		}
	}
}

int InstallAndroidApp(String^ sId, String^ sPcFileName)
{
	int nRet = ERROR_INVALID_PARAMETER;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" install -r -g --bypass-low-target-sdk-block \"{0}\"", sPcFileName);
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	auto retdata = runExe->startRunExe(gcnew String(adbpath), sparm, 30000);
	nRet = retdata->Item1;
	if (nRet != ERROR_SUCCESS)
	{
		sparm = String::Format(" install -r -g  \"{0}\"", sPcFileName);
		if (!String::IsNullOrEmpty(sId))
		{
			sparm = String::Format("-s {0} ", sId) + sparm;
		}
		retdata = runExe->startRunExe(gcnew String(adbpath), sparm, 30000);
		nRet = retdata->Item1;
		if (nRet != ERROR_SUCCESS) {
			for each (String^ s in retdata->Item3->ToArray())
			{
				logIt(s);
			}
		}
	}
	return nRet;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// uninstall app
// 

String^ GetThirdAppList(String^ sId)
{
	StringBuilder^ sb = gcnew StringBuilder();
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" shell pm list packages -3");
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	auto retdata = runExe->startRunExe(gcnew String(adbpath), sparm, 30000);

	if (retdata->Item1 == ERROR_SUCCESS)
	{
		//Sleep(1000);
		for each (String^ s in retdata->Item2->ToArray())
		{
			array<Char>^ chars = { ':' };
			array<String^>^ sps = s->Split(chars, StringSplitOptions::RemoveEmptyEntries);
			if (sps->Length == 2)
			{
				sb->Append(sps[1]);
				sb->Append(";");
			}
		}
	}
	

	return sb->ToString();
}

array<String^>^ RunAdbCommand(String^ sId, String^ sCommanline)
{
	array<String^>^ nRet = gcnew array<String^>(1);
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" shell {0}", sCommanline);
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	int exeret=0;
	auto retdata = runExe->startRunExe(gcnew String(adbpath), sparm, 30000);

	if (retdata->Item1 == ERROR_SUCCESS)
	{
		//rm -fr $EXTERNAL_STORAGE/*
		return retdata->Item2->ToArray();
	}
	else
	{
		nRet[0] = String::Format("runexe return {0}", exeret);
	}
	return nRet;
}

int ShUnInstallAndroidApp(String^ sId, String^ sFile)
{
	int nRet = ERROR_INVALID_PARAMETER;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sPhoneFile = "/data/local/tmp/fdunistallapp.sh";
	String^ sparm = String::Format(" push \"{0}\" {1}", sFile, sPhoneFile);
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	auto retdata = runExe->startRunExe(gcnew String(adbpath), sparm, 30000);
	nRet = retdata->Item1;
	if (nRet == ERROR_SUCCESS)
	{
		sparm = String::Format(" shell chmod 777 {0};{0}", sPhoneFile);
		if (!String::IsNullOrEmpty(sId))
		{
			sparm = String::Format("-s {0} ", sId) + sparm;
		}
		retdata = runExe->startRunExe(gcnew String(adbpath), sparm, 30000);
		nRet = retdata->Item1;
		if (nRet == ERROR_SUCCESS)
		{
			for each (String^ s in retdata->Item2->ToArray())
			{
				logIt("clear data logs:==>" + s);
			}
		}
	}	return nRet;
}

int UnInstallAndroidApp(String^ sId, String^ ingoreData)
{
	int nRet = ERROR_INVALID_PARAMETER;

	String^ sBundle;
	if (String::IsNullOrEmpty(sBundle))
	{
		sBundle = GetThirdAppList(sId);
	}

	if (String::IsNullOrEmpty(sBundle))
	{
		return ERROR_SUCCESS;
	}

	CRunExe^ runExe = gcnew CRunExe();
	String^ delimStr = ";:";
	array<Char>^ delimiter = delimStr->ToCharArray();
	array<String^>^ appids = sBundle->Split(delimiter, StringSplitOptions::RemoveEmptyEntries);

	array<String^>^ ingnoreappids = ingoreData->Split(delimiter, StringSplitOptions::RemoveEmptyEntries);
	List<String^>^ configignore = GetIgnoreApp("pmandroid_app");
	configignore->AddRange(ingnoreappids);

	for each(String^ ss in appids)
	{
		//if (Array::IndexOf(ingnoreappids, ss) > -1)
		if (configignore->Contains(ss))
		{
			logIt("ignore app " + ss);
			if (String::Compare(ss, "com.futuredial.androidclean", true) == 0) {
				String^ sparm = " shell \"dumpsys package com.futuredial.androidclean | grep version\"";
				if (!String::IsNullOrEmpty(sId))
				{
					sparm = String::Format("-s {0} ", sId) + sparm;
				}
				nRet = runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000);
			}
			continue;
		}
		String^ sparm = String::Format(" uninstall {0}", ss);
		if (!String::IsNullOrEmpty(sId))
		{
			sparm = String::Format("-s {0} ", sId) + sparm;
		}
		nRet = runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000);
		if (nRet == ERROR_SUCCESS)
		{
			nRet = ERROR_NOT_FOUND;
			for each (String^ s in runExe->getExeStdOut()->ToArray())
			{
				Console::WriteLine(ss + "=" + s);
				logIt(ss+"="+s);
				if (String::Compare(s, "Success", TRUE) == 0)
				{
					nRet = ERROR_SUCCESS;
					break;
				}
			}
		}

		//if (nRet != ERROR_SUCCESS) break;
	}


	return nRet;
}


/////////////////////////////////////////////////////////////////////////////////////////////
//Device File Exist
//

Boolean PhoneAndroidFileExist(String^ sId, String^ sPhoneFileName)
{
	Boolean bRet = FALSE;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" shell ls {0}", sPhoneFileName);
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	if (runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000) == ERROR_SUCCESS)
	{
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt(s);
			if (String::Compare(s, sPhoneFileName, TRUE) == 0)
			{
				bRet = TRUE;
				break;
			}
		}
	}
	else
	{
		for each (String^ s in runExe->getExeStdOut())
		{
			logIt(s);
		}
	}

	return bRet;
}

////////////////////////////////////////////////////////////////////////////////////////
///upload file
//
int UploadAndroidFile(String^ sId, String^ sPcFileName, String^ sDevFileName)
{
	int nRet = ERROR_INVALID_PARAMETER;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" push \"{0}\" \"{1}\"", sPcFileName, sDevFileName);
	if (nEchoType == 1)
	{
		array<String^>^ filelines = System::IO::File::ReadAllLines(sPcFileName);
		String^ sData = String::Join("\\n", filelines);
		logIt(sData);
		sparm = String::Format(" shell \"echo '{0}' > \"{1}\" \"", sData, sDevFileName);
	}
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	if ((nRet = runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000)) == ERROR_SUCCESS)
	{
		if (nEchoType == 1)
			return nRet;
		nRet = ERROR_NOT_FOUND;
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt(s);	
			if (s->IndexOf("failed to copy") >= 0 || s->IndexOf("Permission denied") > 0)
			{
				nRet = ERROR_ACCESS_DENIED;
				break;
			}
			else {
				//346 KB/s (2484 bytes in 0.007s)
				Regex^ rx = gcnew Regex("\\d+\\s.*?[/]s\\s\\(\\d+\\s.*?s\\)",
					RegexOptions::Compiled | RegexOptions::IgnoreCase);
				if (rx->IsMatch(s))
				{
					nRet = ERROR_SUCCESS;
					break;
				}
			}
		}
	}
	else
	{
		for each (String^ s in runExe->getExeStdErr())
		{
			logIt(s);
		}
	}
	return nRet;
}

//////////////////////////////////////////////////////////////////////////////////////////////
//download file
//

int DownloadAndroidFile(String^ sId, String^ sPcFileName, String^ sDevFileName)
{
	int nRet = ERROR_INVALID_PARAMETER;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" pull \"{1}\" \"{0}\"", sPcFileName, sDevFileName);
	if (nEchoType == 1)
	{
		sparm = String::Format(" shell cp {1} /data/local/tmp ", sPcFileName);
		if (!String::IsNullOrEmpty(sId))
		{
			sparm = String::Format("-s {0} ", sId) + sparm;
		}
		if ((nRet = runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000)) == 0)
		{
			sparm = String::Format(" pull \"/data/local/tmp/{1}\" \"{0}\"", sPcFileName, System::IO::Path::GetFileName(sDevFileName));
		}
		else
			return nRet;
	}
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	if ((nRet = runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000)) == ERROR_SUCCESS)
	{
		if (nEchoType == 1)
			return nRet;
		nRet = ERROR_NOT_FOUND;
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt(s);
			if (s->IndexOf("Permission denied") > 0)
			{
				nRet = ERROR_ACCESS_DENIED;
				break;
			}
			else if (s->IndexOf("pulled")) {///sdcard/view.xml: 1 file pulled.
				nRet = ERROR_SUCCESS;
				break;
			}
			else {
				//346 KB/s (2484 bytes in 0.007s)
				Regex^ rx = gcnew Regex("\\d+\\s.*?[/]s\\s\\(\\d+\\s.*?s\\)",
					RegexOptions::Compiled | RegexOptions::IgnoreCase);
				if (rx->IsMatch(s))
				{
					nRet = ERROR_SUCCESS;
					break;
				}
			}
		}
	}
	else
	{
		for each (String^ s in runExe->getExeStdErr())
		{
			logIt(s);
		}
	}
	return nRet;
}
/////////////////////////////////////////////////////////////////////////////////////////
//Dump UI File
//

int AndroidDumpUIXml(String^ sId, String^ sFileName) {
	int bRet = ERROR_NOT_FOUND;
	CRunExe^ runExe = gcnew CRunExe();//adb shell uiautomator dump /sdcard/view.xml

	String^ sparm = String::Format(" shell uiautomator dump \"/data/local/tmp/{0}\"",  System::IO::Path::GetFileName(sFileName));
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	if ((bRet = runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000)) == ERROR_SUCCESS)
	{

	}
	else
	{
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt(s);
		}
	}

	if (bRet == ERROR_SUCCESS) {
		bRet = DownloadAndroidFile(sId, sFileName, String::Format("/data/local/tmp/{0}", System::IO::Path::GetFileName(sFileName)));
	}
	return bRet;
}

//// get x.y
//// adb shell input tap x y

int AndroidTapScreenBy(String^ sId, String^ xmlfilter) {
	String^ sFilename= System::IO::Path::Combine(Environment::ExpandEnvironmentVariables("%APSTHOME%"),"temp", System::IO::Path::GetRandomFileName());
	sFilename = System::IO::Path::ChangeExtension(sFilename, ".xml");
	int nRet = AndroidDumpUIXml(sId, sFilename);
	if (!::IO::File::Exists(sFilename)) {
		logIt("File not exist");
		return ERROR_NOT_FOUND;
	}
	array<wchar_t>^ id = { ';' };
	array<String^>^ filters = xmlfilter->Split(id, StringSplitOptions::RemoveEmptyEntries);

	XmlDocument^ doc = gcnew XmlDocument();
	doc->Load(sFilename);
	for each(String^ sf in filters) {
		XmlNode^ n = doc->SelectSingleNode(sf);
		if (n != nullptr)
		{
			XmlAttribute^ a = n->Attributes["bounds"];
			if (a != nullptr)
			{
				Regex^ r = gcnew Regex("\\[(\\d+),(\\d+)\\]\\[(\\d+),(\\d+)\\]");
				Match^ m = r->Match(a->Value);
				if (m->Success)
				{
					int x = -1, y = -1;
					int xx = -1, yy = -1;
					logIt("start get x and y");
					//m->Groups[1].Value
					//m->Groups[2].Value
					Int32::TryParse(m->Groups[1]->Value, x);
					Int32::TryParse(m->Groups[2]->Value, y);
					Int32::TryParse(m->Groups[3]->Value, xx);
					Int32::TryParse(m->Groups[4]->Value, yy);

					RunAdbCommand(sId, String::Format("input tap {0} {1}", int((x + xx) / 2), int((y + yy) / 2)));
					nRet = ERROR_SUCCESS;
					break;
				}

			}
		}
	}
	if (System::IO::File::Exists(sFilename)) {
		try {
			System::IO::File::Delete(sFilename);
		}
		catch (Exception^) {

		}
	}
	return ERROR_SUCCESS;
}


/////////////////////////////////////////////////////////////////////////////////////////
//File Delete
//
int PhoneAndroidFileDelete(String^ sId, String^ sPhoneFileName)
{
	int bRet = ERROR_NOT_FOUND;
	CRunExe^ runExe = gcnew CRunExe();
	String^ sparm = String::Format(" shell rm -f {0}", sPhoneFileName);
	if (!String::IsNullOrEmpty(sId))
	{
		sparm = String::Format("-s {0} ", sId) + sparm;
	}
	if ((bRet = runExe->ProcessRunExe(gcnew String(adbpath), sparm, 30000)) == ERROR_SUCCESS)
	{
		
	}
	else
	{
		for each (String^ s in runExe->getExeStdOut()->ToArray())
		{
			logIt(s);
		}
	}

	return bRet;
}

